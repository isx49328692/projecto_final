﻿<a name="_8t6q0pcr1ziy"></a>Implementación de un servidor FreeIPA en un entorno educativo

<a name="_8wfqgngj9orq"></a>Integración de servicios de DNS, LDAP, Kerberos y NFS con FreeIPA



<a name="_sirfy1cefypx"></a><a name="_fqygkqx7kfc2"></a><a name="_lcf2iw3pvdqv"></a>**Juan Pierre Sánchez Cercado**

ÍNDICE

[**Especificaciones y objetivos del proyecto	2**](#_hsvnlvsr7tjf)**

[**Historia de FreeIPA	3**](#_34yofgnzgrtq)

[**¿Qué es FreeIPA?	3**](#_dpjw8zeddfwp)

[**Funcionamiento de FreeIPA	4**](#_s4i7mcj454b1)

[Puntos clave de su funcionamiento:	4](#_o6m776le0kjf)

[**Funciones de FreeIPA	5**](#_c2nrgvkv2q49)

[**Implementación	6**](#_3jbjtc5bzxrq)

[**Desarrollo práctico del proyecto	7**](#_ckacnlu5h259)

[Parte común del host cliente y servidor	7](#_kr6egpp24lq9)

[Configuración IP	8](#_a31n861cao0u)

[Configuración hostname	9](#_psh2cxfin0mj)

[Configuración /etc/hosts y /etc/resolv.conf	10](#_uwrc51wurwh3)

[Configuración del servidor	11](#_if0fixu56hv6)

[Configuración DNS	11](#_cyxhdc8vznt6)

[Configuración ipa-server	13](#_iuxq3wrapb5w)

[Creación de usuario y grupos	14](#_o7zctpcpxx0p)

[Configuración ipa-client	17](#_chogy2yd1qc4)

[Configuración NFS	19](#_armot63rybkn)

[servicio NFS	19](#_2re90nugo146)

[Servidor NFS	20](#_twv3kkwajbos)

[Cliente NFS	21](#_65sjw9cqmkmz)

[**Conclusiones	22**](#_kaewmnz47gj5)

[**Bibliografía	23**](#_tgwfzdeaj9w9)


# <a name="_hsvnlvsr7tjf"></a>Especificaciones y objetivos del proyecto

Este proyecto tiene por objetivo implementar un servidor FreeIPA en un ficticio entorno educativo, consta de un servidor el cual tendrá integrado los servicios DNS, LDAP, Kerberos y NFS. Se disponen 4 hosts clientes que deben poder conectarse al servidor para iniciar sesión con los usuarios ipa creados en el servidor y autenticarse mediante kerberos.

El entorno en el que estará funcionando será el siguiente, el dominio sera jpsanchez.edu y el servidor tendrá por hostname “ipa.jpsanchez.edu” mientras que cada host será host1, host2, host3 y host4. Las IPs de cada host serán 192.168.10.2 para el servidor IPA, y de los hosts clientes 192.168.10.10 - 13.

Cómo estará integrado kerberos ester tomará el RELAME del dominio por lo que será JPSANCHEZ.EDU.

Los usuarios serán juan y pierre para el grupo de profesores, isai y jack para el grupo de alumnos y rosa para el grupo de secretaria. Cada usuario deberá tener su home y deberá tener acceso a los recurso apuntes, notas y matrícula dependiendo de su grupo
# <a name="_34yofgnzgrtq"></a>Historia de FreeIPA

FreeIPA es un proyecto de código abierto que nació en 2008 gracias al proyecto Fedora y a RedHat. El objetivo principal de este proyecto era ofrecer una solución completa y fácil de usar para la administración de identidades y políticas de seguridad en entornos de servidores Linux.

Desde su creación, FreeIPA ha evolucionado y se ha convertido en una herramienta esencial para cualquier organización que desee gestionar sus identidades y servicios de forma segura y centralizada.

# <a name="_dpjw8zeddfwp"></a>¿Qué es FreeIPA?

FreeIPA, cuyo nombre significa Identidad, Políticas, Auditoría Libre (en inglés, Free Identity, Policy, Audit), es una plataforma/solución de gestión de identidades y accesos (IAM, por sus siglas en inglés) de código abierto que ofrece servicios de autenticación, autorización y contabilidad centralizada para entornos de servidores Linux, es decir, proporciona administración centralizada de usuarios, grupos, host y políticas de seguridad. Esta herramienta permite gestionar y controlar el acceso de los usuarios a los recursos de la organización, ya sean sistemas, aplicaciones o servicios de red.

FreeIPA es una solución integral que combina varios componentes, como un servidor de directorio basado en LDAP, un sistema de autenticación Kerberos, un servicio de certificación PKI y herramientas de administración basadas en web. Todo esto se integra en una única solución fácil de usar y administrar. (acabar de entender)

Como se ha comentado anteriormente, FreeIPA también proporciona una interfaz de línea de comandos y una interfaz web de usuario para administrar los servicios de FreeIPA, y se integra con otros servicios de Linux, como Samba y SSH.


# <a name="_s4i7mcj454b1"></a>Funcionamiento de FreeIPA

FreeIPA funciona como un conjunto de servicios interconectados que trabajan juntos para proporcionar una solución de IAM completa. El servicio principal es el servidor de directorio LDAP, que almacena y gestiona la información de las identidades de los usuarios, los grupos y los sistemas. También es responsable de la autenticación de los usuarios y de la autorización de los accesos a los recursos de la organización.

El sistema de autenticación Kerberos es el responsable de generar y gestionar las credenciales de los usuarios, proporcionando un alto nivel de seguridad y evitando ataques de suplantación de identidad. El servicio PKI, por su parte, ofrece un sistema de certificación que permite la autenticación de los usuarios y sistemas mediante el uso de certificados digitales. (¿Kerberos?)

Además, FreeIPA ofrece herramientas de administración basadas en web que permiten a los administradores de sistemas gestionar y configurar los diferentes servicios de IAM de forma centralizada y sencilla.

## <a name="_o6m776le0kjf"></a>Puntos clave de su funcionamiento:

FreeIPA funciona mediante la integración de varios servicios de red y seguridad en una solución integrada. Los servicios que se incluyen en FreeIPA son:

- LDAP: FreeIPA utiliza LDAP (Lightweight Directory Access Protocol) para almacenar y administrar información de identidad y autenticación, como usuarios, grupos y políticas de seguridad.

- Kerberos: FreeIPA utiliza Kerberos para autenticar usuarios y servicios en la red. Kerberos proporciona un mecanismo seguro para la autenticación y la transmisión de claves de sesión.

- DNS: FreeIPA utiliza DNS (Domain Name System) para resolver nombres de host y proporcionar servicios de autenticación y autorización basados en nombres.

- Certificados de seguridad (PKI): FreeIPA utiliza certificados de seguridad para proporcionar autenticación y cifrado de datos. FreeIPA incluye una Autoridad de Certificación (CA) interna que emite certificados de seguridad para usuarios, hosts y servicios.

## <a name="_c2nrgvkv2q49"></a>Funciones de FreeIPA

Entre las funciones principales de FreeIPA se encuentran:

- Autenticación centralizada: FreeIPA permite a los usuarios autenticarse de forma centralizada en todos los servicios de la organización, lo que facilita la gestión de contraseñas y aumenta la seguridad.

- Gestión de identidades y acceso: FreeIPA proporciona herramientas para gestionar y controlar el acceso de los usuarios a los diferentes recursos de la organización.

- Administración centralizada: FreeIPA ofrece herramientas de administración basadas en web que permiten a los administradores de sistemas gestionar y configurar los diferentes servicios de IAM de forma centralizada y sencilla.

- Sistema de autenticación Kerberos: FreeIPA utiliza el sistema de autenticación Kerberos para proporcionar un alto nivel de seguridad y evitar ataques de suplantación de identidad.

- Servicio de directorio basado en LDAP: FreeIPA utiliza un servidor de directorio basado en LDAP para almacenar y gestionar la información de las identidades de los usuarios, los grupos y los sistemas.

# <a name="_1jz1x4cwyk0e"></a>
# <a name="_3jbjtc5bzxrq"></a>Implementación

Para implementar FreeIPA, se requiere un servidor CentOS 8 (o una distribución de Linux similar) y seguir una serie de pasos de configuración y personalización. En general, la implementación de FreeIPA se divide en los siguientes pasos:

Instalación del servidor CentOS: La primera tarea es instalar un servidor CentOS 8 en la máquina que se utilizará como servidor FreeIPA. El servidor debe estar configurado con una dirección IP estática y un nombre de host válido.

Instalación de FreeIPA: Una vez que el servidor CentOS base esté en su lugar, el siguiente paso es instalar FreeIPA. Esto se puede hacer utilizando el gestor de paquetes de la distribución de Linux o mediante la descarga del paquete de instalación de FreeIPA desde el sitio web oficial de FreeIPA.

Configuración de FreeIPA: Después de instalar FreeIPA, el siguiente paso es configurar el sistema según sus necesidades específicas. Esto incluye la configuración de los servicios de DNS, LDAP y Kerberos, la creación de usuarios y grupos y la configuración de políticas de acceso y seguridad.
# <a name="_ckacnlu5h259"></a>Desarrollo práctico del proyecto
## <a name="_kr6egpp24lq9"></a>Parte común del host cliente y servidor

Será desarrollado en Virtual Machine Manager, primero se debe descargar un imagen iso de CentOS 8 y crear 5 hosts con al menos 2 CPU, 2000 MiB de memoria.

Al instalar CentOS 8 nos encontramos con un problema de repositorios, CentOS ya no los actualiza y debemos hacer unos cambios para poder instalar FreeIPA.

Lo primero:

**/etc/yum.repos.d/**

**sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-\***

**sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-\***

Con esto cambiamos el origen de los repositorios.

Ahora podremos realizar un update pero seguimos sin poder instalar FreeIPA, para hacer primero debemos de instalar el repositorio siguiente:

**dnf -y install @idm:DL1**

**dnf -y install freeipa-server bind nds-ldap**

En el caso del host cliente:

`	`**dnf -y install freeipa-client –mkdhomedir**

Ahora ya lo tenemos instalado pero previo a ejecutar su configuración debemos realizar una configuración previa en el servidor, como es la IP, el hostname y el DNS.
### <a name="_a31n861cao0u"></a>Configuración IP

Para la configuración estática de la IP debemos editar el archivo **/etc/sysconfig/network-scripts/ifcfg-enp1s0**

En el caso del server la configuración será la siguiente:

**TYPE="Ethernet"**

**Proxy\_METHOD="none"**

**BROWSER\_ONLY="no"**

**#BOOTPROTO="dhcp"**

**DEFROUTE="yes"**

**IPV4\_FAILURE\_FATAL="no"**

**IPV6INIT="yes"**

**IPV6AUTOCONF="yes"**

**IPV6\_DEFROUTE="yes"**

**IPV6\_FAILURE\_FATAL="no"**

**IPV6\_ADDR\_GEN\_MODE="stable-privacy"**

**NAME="enp1s0"**

**ONBOOT="yes"**

**BOOTPROTO="yes"**

**IPADDR=192.168.10.2**

**NETMASK=255.255.255.0**

**DNS1=8.8.8.8**

**DNS2=8.8.4.4**


Podemos ver que configuramos la IP con la directiva **IPADDR=192.168.10.2**  y que comentamos cambiamos **BOOTPROTO=”dhcp”** a **BOOTPROTO=”none”** o **static.**

En el caso del cliente la cosa cambia un poco:

`	`**TYPE="Ethernet"**

**Proxy\_METHOD="none"**

**BROWSER\_ONLY="no"**

**#BOOTPROTO="dhcp"**

**DEFROUTE="yes"**

**IPV4\_FAILURE\_FATAL="no"**

**IPV6INIT="yes"**

**IPV6AUTOCONF="yes"**

**IPV6\_DEFROUTE="yes"**

**IPV6\_FAILURE\_FATAL="no"**

**IPV6\_ADDR\_GEN\_MODE="stable-privacy"**

**NAME="enp1s0"**

**ONBOOT="yes"**

**BOOTPROTO=none**

**IPADDR=192.168.10.10**

**NETMASK=255.255.255.0**

**DNS1=192.168.10.2**

Con esta configuración del cliente podemos ver que la ip estatica del **host1** será **192.168.10.10** además hemos configurado el la **IP** del **DNS**, ahora apunta al servidor ipa **DNS1=192.168.10.2**.

El proceso del cliente lo repetimos en cada host cliente teniendo en cuenta que la IP debe cambiar (.11, .12, .13).


### <a name="_psh2cxfin0mj"></a>Configuración hostname

Por el momento tenemos configurada la IP, ahora debemos configurar el hostname, esto se hace de una manera sencilla.

En el servidor editaremos el archivo **/etc/hostname** y reescribimos:

`	`**ipa.jpsanchez.edt**

En los clientes editaremos el mismo archivo pero cambiando su hostname por **host1.jpsanchez.edu, host2.jpsanchez.edu, host3.jpsanchez.edu, host4.jpsanchez.edu**. Cada uno en su host.

Este paso es importante pues al momento de instalar y configurar freeipa extrae estos datos y por ello deben estar bien configurados.
### <a name="_uwrc51wurwh3"></a>Configuración /etc/hosts y /etc/resolv.conf

Este es el último paso de la configuración común entre el servidor y el cliente

en el servidor debemos de editar el archivo **/etc/hosts** e indicar:

**127.0.0.1	localhost localhost.localdomain	localhost4 localhost4.localdoamin5**

**::1	localhost	localhost.localdomain	localhost6	localhost6.localdoamin6**

**192.168.10.2	ipa.jpsanchez.edu	ipa**

**192.168.10.10	host1.jpsanchez.edu	host1**

**192.168.10.11	host2.jpsanchez.edu	host2**

**192.168.10.12	host3.jpsanchez.edu	host3**

**192.168.10.13	host4.jpsanchez.edu	host4**

Y para el host1 (como ejemplo de cliente):

**127.0.0.1	localhost	localhost.localdomain	localhost4	localhost4.localdomain4**

**::1	localhost	localhost.localdomain	localhost6	localhost6.localdomain6**

**192.168.10.2	ipa.jpsanchez.edu	ipa**

**192.168.10.11	host2.jpsanchez.edu	host1**

**192.168.10.12	host3.jpsanchez.edu	host2**

**192.168.10.13	host4.jpsanchez.edu	host3**

Ahora para el archivo **/etc/resolv.conf** en el servidor quedará como:

**# Generated by NetworkManager**

**search		jpsanchez.edu**

**nameserver	127.0.0.1**

En los clientes será:

**# Gnereated by NetworkManager**

**search	jpsanchez.edu**

**nameserver	192.168.10.2**
## <a name="_if0fixu56hv6"></a>Configuración del servidor
### <a name="_cyxhdc8vznt6"></a>Configuración DNS

En el servidor debemos configurar correctamente el DNS ya que nuestros clientes nos usaran como servidor DNS.

Primero debemos debemos configurar el archivo **/etc/named.conf** e introducir la zona directa e inversa:

**zone jpsanchez.edu IN {**

**type	master;**

**file	“jpsanchez.edu.zone”;**

**};**

**zon3 10.168.192.in-addr.zone IN {**

`	`**type	master;**

`	`**file	“jpsanchez.edu.zone”;**

**};**


La configuración de los archivos de zona **/var/named/**:

Del jpsanchez.edu.zone:

**$TTL	86400**

**@	IN	SOA	ipa.jpsanche.edu	admin.jpsanchez.edu	(**

`			`**20230613	;**

`			`**3600	;**

`			`**1800	;**

`			`**604800	;**

`			`**86400	;**

**)**

**@	IN	NS	ipa.jpsanchez.edu.**

**ipa	IN	A	192.168.10.2**

**host1	IN	A	192.168.10.10**

**host2	IN	A	192.168.10.11**

**host3	IN	A	192.168.10.12**

**host4	IN	A	192.168.10.13**

Del 10.168.192.in-addr.arpa.zone:

**$TTL	86400**

**@	IN	SOA	ipa.jpsanche.edu	admin.jpsanchez.edu	(**

`			`**20230613	;**

`			`**3600	;**

`			`**1800	;**

`			`**604800	;**

`			`**86400	;**

**)**

**@	IN	NS	ipa.jpsanchez.edu.**

**2	IN	PTR	ipa.jpsanchez.edu.**

**10	IN	PTR	host1.jpsanchez.edu.**

**11	IN	PTR	host2.jpsanchez.edu.**

**12	IN	PTR	host3.jpsanchez.edu.**

**13	IN	PTR	host4.jpsanchez.edu.**


## <a name="_iuxq3wrapb5w"></a>Configuración ipa-server

Para comenzar con la configuración final del servidor debemos ejecutar el comando **ipa-server-install**, al ejecutarlo iniciará la configuración ipa-server la cual debería ser sencilla si hemos hecho correctamente toda la configuración previa.

Entre otras preguntas, las que importan para nuestra configuración son:

- Hostname: nos sugerirá el que habíamos configurado de modo que entre y seguimos.

- Nombre de dominio: Lo extrae del hostname, enter.

- REALM: Al igual que el nombre de dominio, también lo extrae del hostname, en este caso en mayúsculas.

- Directory Manager password: Contraseña para el administrador de directorio.

- Password for the IPA admin user: Contraseña para el usuario admin


Respondiendo a las preguntas las preguntas tendremos hecha la configuración del servidor FreeIPA server.
## <a name="_o7zctpcpxx0p"></a>Creación de usuario y grupos

Ahora con la configuración hecha procedemos a crear los grupos y usuarios, en mi caso lo haré a través de un script **user.sh:**

**#!/bin/bash**

**users="juan pierre isai jack rosa"**

**# obtener dominio**

**domain=$(domainname)**

**# Credenciales**

**echo -e "ipaadmin\n" | kinit admin && echo "OK INIT"**

**# Crear usuarios**

**for user in $users**

**do**

`	`**echo -e"k$user\nk$user" | ipa user-add $user --first=$user --last=$user --shell=/bin/bash --email=$user@$doamin**

**done**

**# grupos**

**grups="profesores alumnos secretaria"**

**# Credenciales**

**echo -e "ipaadmin\n" | kinit admin && echo "OK INIT"**

**# Crear grupos**

**for grup in $grups**

**do**

`	`**ipa group-add $grup**

**done**

**# Credenciales**

**echo -e "ipaadmin\n" | kinit admin && echo "OK INIT"**

**# Asiganr usuarios a grupos**

**ipa group-add-member profesores --users=juan**

**ipa group-add-member profesores --users=pierre**

**ipa group-add-member alumnos --users=isai**

**ipa group-add-member alumnos --users=jack**

**ipa group-add-member secretaria --users=rosa**



Cuando se crea un usuario en FreeIPA, se crean automáticamente las entradas correspondientes en los servicios de LDAP y Kerberos, junto con la configuración necesaria para la autenticación y autorización del usuario en el sistema.

A continuación daré una explicación paso a paso de cómo se crea un usuario en FreeIPA y cómo se relaciona con LDAP y Kerberos:

- Creación del usuario en FreeIPA:

- Utiliza el comando **ipa user-add** seguido del nombre de usuario para crear un usuario en FreeIPA. Por ejemplo: **ipa user-add nombre\_de\_usuario**

- Puedes agregar varios atributos adicionales al usuario, como nombre completo, dirección de correo electrónico, grupo principal, etc. Estos atributos ayudan a definir las propiedades y permisos del usuario en el sistema.

- Durante la creación del usuario, también se establece una contraseña para el usuario. La contraseña se almacena de forma segura en el sistema.

- Integración con LDAP:

- FreeIPA utiliza un directorio LDAP para almacenar información sobre los usuarios y grupos del sistema.

- Cuando creas un usuario en FreeIPA, se crea automáticamente una entrada correspondiente en el directorio LDAP.

- La entrada LDAP contiene atributos como el nombre de usuario, nombre completo, dirección de correo electrónico, UID (identificador único), GID (identificador de grupo), entre otros.

- Esta integración con LDAP permite el acceso y la administración de usuarios y grupos mediante consultas LDAP estándar.



- Integración con Kerberos:

- FreeIPA utiliza el servicio de autenticación Kerberos para permitir a los usuarios autenticarse en el sistema y acceder a los recursos protegidos.

- Cuando creas un usuario en FreeIPA, se crea automáticamente una principal de Kerberos para ese usuario.

- La principal de Kerberos se basa en el nombre de usuario y el reino de Kerberos asociado con el dominio de FreeIPA.

- La principal de Kerberos se utiliza para la autenticación del usuario en el sistema, lo que le permite acceder a servicios protegidos por Kerberos.
## <a name="_chogy2yd1qc4"></a>Configuración ipa-client

Ahora debemos configurar los hosts clientes, ejecutando el comando **ipa-client-install --mkhomedir**, con esta opción hacemos que se cree automáticamente los directorios de inicio (home) de los usuarios en los clientes cuando inician sesión por primera vez.

El comando hará que inicie un procedimiento de instalación y configuración automatizada y si la sincronización sale bien te mostrará una salida con los datos del servidor al que pretende conectarse, te preguntará si son correctos y si deseas continuar con esa configuración, además nos se nos hará una serie de preguntas para finalizar la configuración, el proceso sería como el siguiente:

- Descubrimiento del servidor:

- El cliente intentará descubrir automáticamente el servidor FreeIPA en la red. Si no se puede hacer automáticamente, se te solicitará ingresar manualmente la dirección IP o el nombre de host del servidor.

- Autenticación y autorización:

- Se  pedirá un nombre de usuario y contraseña con privilegios de administrador en el dominio de FreeIPA. Esto es necesario para configurar correctamente el cliente.

- Configuración de DNS:

- Nos pedirá configurar el cliente para usar el servidor DNS de FreeIPA. Puedes optar por usar la configuración predeterminada o ingresar manualmente la dirección IP del servidor DNS.

- Configuración de Kerberos:

- Se te preguntará si deseas configurar el cliente para autenticarse con el servidor Kerberos de FreeIPA. La configuración predeterminada suele ser "Sí" para habilitar la autenticación Kerberos.

- Configuración de NTP:

- Se te preguntará si deseas configurar el cliente para sincronizar el tiempo con el servidor NTP (Network Time Protocol) de FreeIPA. Puedes optar por usar la configuración predeterminada o ingresar manualmente la dirección IP o el nombre de host del servidor NTP.

- Configuración de certificados:

- Se te preguntará si deseas configurar el cliente para confiar en los certificados emitidos por FreeIPA. Esto es necesario para establecer una comunicación segura entre el cliente y el servidor.

- Configuración del inicio de sesión:

- Se te pedirá si deseas configurar el inicio de sesión para que los usuarios del cliente inicien sesión utilizando las credenciales de FreeIPA. Esto permite la autenticación centralizada y el acceso a los recursos protegidos.

- Estas son algunas de las preguntas comunes que podrías encontrar durante el proceso de instalación y configuración del cliente con ipa-client-install. Las respuestas a estas preguntas dependen del entorno y de tus necesidades específicas.


## <a name="_armot63rybkn"></a>Configuración NFS
### <a name="_2re90nugo146"></a>servicio NFS

El servicio NFS (Network File System) es un protocolo que permite a los sistemas operativos Unix y Linux compartir sistemas de archivos a través de una red. NFS facilita el acceso remoto a archivos y directorios en otros sistemas, como si estuvieran siendo accedidos localmente.

El funcionamiento básico del servicio NFS implica los siguientes componentes:

Servidor NFS: 

Es el sistema que comparte los recursos (archivos y directorios) a través de NFS. El servidor NFS exporta los sistemas de archivos que se desean compartir y define los permisos de acceso para los clientes. Estos recursos compartidos son accesibles a través de una dirección IP y una ruta en la red.

Cliente NFS: 

Es el sistema que accede a los recursos compartidos por el servidor NFS. Los clientes NFS montan los sistemas de archivos remotos en su propio sistema de archivos local, lo que les permite acceder y utilizar los archivos y directorios como si estuvieran almacenados localmente.

Protocolo NFS: 

Es el protocolo de red utilizado por el servicio NFS para la comunicación entre el servidor y el cliente. El protocolo NFS permite la transferencia de datos y los comandos de control necesarios para acceder y manipular los archivos y directorios compartidos.

Cuando un cliente NFS desea acceder a un archivo o directorio remoto, sigue los siguientes pasos:

El cliente envía una solicitud al servidor NFS para acceder a un recurso compartido específico.

El servidor NFS verifica los permisos y autentica al cliente para garantizar que tenga los derechos necesarios para acceder al recurso solicitado.

Si el cliente tiene los permisos adecuados, el servidor NFS responde con la información del recurso solicitado, como los atributos del archivo o una lista de directorios y archivos en un directorio determinado.

El cliente monta el sistema de archivos remoto en su propio sistema de archivos local, lo que permite que los archivos y directorios remotos se vean y manipulen como si fueran locales.

A partir de este momento, el cliente puede leer, escribir o ejecutar los archivos y directorios remotos como si estuvieran almacenados localmente en su propio sistema.

El servicio NFS utiliza el modelo cliente-servidor para proporcionar un acceso transparente a los sistemas de archivos remotos. Esto permite compartir y acceder a archivos y directorios entre diferentes sistemas de manera eficiente y conveniente en una red.

### <a name="_twv3kkwajbos"></a>Servidor NFS

En este caso en el servidor creamos los directorios que deseamos exportar

`	`**mkdir /apuntes /notas /matriculas**

`	`**mkdir /home/profesores**

`	`**mkdir /home/alumnos**

`	`**mkdir /home/secretaria**

`	`**mkdir /apuntes**

`	`**mkdir /notas**

`	`**mkdir /matriculas**

Configuramos bien los permisos:

`	`**sudo chown -R root:alumnos /apuntes**

**sudo chmod 770 /apuntes**

**sudo chown -R root:profesores /notas**

**sudo chmod 770 /notas**

**sudo chown -R root:secretaria /matriculas**

**sudo chmod 770 /matriculas**

Para exportar desde el servidor debemos editar el archivo **/etc/exports**:



**/home/profesores \*(rw,sync,no\_root\_squash)**

**/home/alumnos \*(rw,sync,no\_root\_squash)**

**/home/secretaria \*(rw,sync,no\_root\_squash)**

**/apuntes \*(rw,sync,no\_root\_squash)**

**/notas \*(rw,sync,no\_root\_squash)**

**/matriculas \*(rw,sync,no\_root\_squash)**


### <a name="_65sjw9cqmkmz"></a>Cliente NFS

Ahora desde el cliente creamos los directorios donde vayamos a montar los recursos.

Editamos el archivo **/etc/fstab:**

`	`**192.168.10.2:/home/profesores /home/profesores nfs defaults 0 0**

**192.168.10.27:/home/alumnos /home/alumnos nfs defaults 0 0**

**192.168.10.2:/home/secretaria /home/secretaria nfs defaults 0 0**

**192.168.10.2:/apuntes /home/profesores/apuntes nfs defaults 0 0**

**192.168.10.2:/apuntes /home/alumnos/apuntes nfs defaults 0 0**

**192.168.10.2:/apuntes /home/secretaria/apuntes nfs defaults 0 0**

**192.168.10.2:/notas /home/profesores/notas nfs defaults 0 0**

**192.168.10.2:/notas /home/secretaria/notas nfs defaults 0 0**

**192.168.10.2:/matriculas /home/secretaria/matriculas nfs defaults 0 0**

Para montarlo todo:

`	`**mount -a**


# <a name="_kaewmnz47gj5"></a>Conclusiones

` `El uso de FreeIPA y el servicio NFS brinda una solución robusta y completa para la gestión de identidades y el acceso a recursos compartidos en una red. FreeIPA proporciona un conjunto de servicios integrados, como LDAP y Kerberos, que permiten una administración centralizada de usuarios, grupos y políticas de seguridad. Además, la integración de NFS facilita el acceso remoto a sistemas de archivos compartidos, permitiendo a los usuarios acceder y utilizar archivos y directorios como si estuvieran almacenados localmente.

Mediante la implementación de FreeIPA, se puede establecer una infraestructura de identidad sólida que simplifica la administración de usuarios y garantiza un acceso seguro a los recursos de la red. Los usuarios pueden autenticarse de manera segura y acceder a sus respectivos directorios de inicio y otros recursos compartidos asignados a sus grupos.

El uso de NFS agrega una capa adicional de flexibilidad y eficiencia al permitir el montaje remoto de sistemas de archivos compartidos. Los usuarios pueden acceder y manipular archivos y directorios remotos como si estuvieran almacenados localmente en sus sistemas. Esto facilita la colaboración y el intercambio de datos en entornos de red.

En resumen, la combinación de FreeIPA y NFS brinda una solución integral para la gestión de identidades y el acceso a recursos compartidos en una red. Proporciona seguridad, control de acceso y facilidad de uso, lo que contribuye a una administración eficiente y una mejor experiencia de usuario en entornos empresariales y educativos.

# <a name="_tgwfzdeaj9w9"></a>Bibliografía

<https://computingforgeeks.com/how-to-install-and-configure-freeipa-server-on-rhel-centos-8/?utm_content=cmp-true>

<https://guidocutipa.blog.bo/como-configurar-servidor-dns-centos-7/>

<https://www.freeipa.org/page/Documentation>

<https://www.youtube.com/watch?v=QBLQ-1pp-w8>

<https://fedoraproject.org/wiki/QA:Testcase_freeipa_trust_server_installation>

<https://freeipa.readthedocs.io/en/latest/workshop/2-client-install.html>

<https://freeipa.readthedocs.io/en/latest/workshop/2-client-install.html>


