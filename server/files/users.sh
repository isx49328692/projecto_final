#!/bin/bash

users="juan pierre isai jack rosa"

# obtener dominio
domain=$(domainname)

# Credenciales
echo -e "ipaadmin\n" | kinit admin && echo "OK INIT"

# Crear usuarios
for user in $users
do
	echo -e"k$user\nk$user" | ipa user-add $user --first=$user --last=$user --shell=/bin/bash --email=$user@$doamin
done

# grupos
grups="profesores alumnos secretaria"

# Credenciales
echo -e "ipaadmin\n" | kinit admin && echo "OK INIT"

# Crear grupos
for grup in $grups
do
	ipa group-add $grup
done

# Credenciales
echo -e "ipaadmin\n" | kinit admin && echo "OK INIT"

# Asiganr usuarios a grupos
ipa group-add-member profesores --users=juan
ipa group-add-member profesores --users=pierre
ipa group-add-member alumnos --users=isai
ipa group-add-member alumnos --users=jack
ipa group-add-member secretaria --users=rosa
